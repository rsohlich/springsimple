/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.app.controller;

import cz.sohlich.app.mongo.Customer;
import cz.sohlich.app.mongo.repo.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Radek
 */
@Controller
public class HelloController {

    @Autowired
    private CustomerRepository repository;

    @RequestMapping("/hello")
    public ModelAndView helloWorld(@RequestParam(value = "name", required = false, defaultValue = "World") String name) {

        Customer c = new Customer("Rada", "Pada");
        c = repository.save(c);

        ModelAndView model = new ModelAndView("helloworld");
        model.addObject("name", c);
        return model;
    }
}
