<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
    <head>
        <%@ page isELIgnored="false" %>
    </head>
    <body>
        <h1>Spring MVC Hello World Example</h1>

        <h2>${name}</h2>

    </body>
</html>